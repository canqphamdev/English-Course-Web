# !/usr/bin/python

# -*- coding: utf8 -*-
from django.contrib import admin
from .models import Teacher, Course, Student

# Register your models here.
class StudentAdmin(admin.ModelAdmin): # không thêm đc
    list_display = ['name','number_phone','address', 'sex', 'date_birth']
    list_filter = ['name', 'date_birth']
    search_fields = ['name']
class TeacherAdmin(admin.ModelAdmin):
    list_display = ['name_teacher', 'number_phone', 'address', 'subjects']

class CourseAdmin(admin.ModelAdmin):
    list_display = ['name_course', 'content']
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Student, StudentAdmin)
